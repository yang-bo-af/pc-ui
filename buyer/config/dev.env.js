const { im, distribution } = require('./index')

module.exports = {
	NODE_ENV: '"development"',
	ENV_CONFIG: '"dev"',
  IM: im,
	DISTRIBUTION: distribution
}
