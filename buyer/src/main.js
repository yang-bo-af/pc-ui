import Vue from 'vue'
import axios from 'axios'
window.axios = axios

import 'babel-polyfill'
import 'nprogress/nprogress.css'
import 'swiper/css/swiper.min.css'
import './assets/styles/normalize.css'
import './assets/styles/base.scss'

import './assets/styles/iconfont/iconfont.css'
import * as EnComponents from './components'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import VueLazyLoad from 'vue-lazyload'
import VueMeta from 'vue-meta'
import VueCropper from 'vue-cropper'
import Viewer from 'v-viewer'
import 'viewerjs/dist/viewer.css'

import App from './App'
import i18n from './lang'
import router from './router'
import './router/router-hook'
import store from './store'
import './utils/vue-layer'
import './utils/vue-element'

import * as filters from './utils/filters'
import mixin from './utils/mixin'

Vue.use(Viewer)
Vue.use(VueAwesomeSwiper)
Vue.use(VueLazyLoad)
Vue.use(VueMeta)
Vue.use(VueCropper)
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})
Object.keys(EnComponents).forEach(key => {
  Vue.component(EnComponents[key].name, EnComponents[key])
})

Vue.mixin(mixin)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  template: '<App/>',
  components: { App }
})
